import sys
import json
import pprint
import re
import requests
import csv

headers = {'Authorization':'27e9b5ae-309c-4912-8e2a-c935653278c5'}
ckan_rest_url = "http://hubdev.edmonton.ca/api/3/action/"

def get_package_list_tag(tag):
    qry = ckan_rest_url+"package_search?fq=tags:{}&rows=1000".format(tag)
    res = requests.get(qry)
    return res.json()['result']['results']
    datasets = []
    for dataset in res.json()['result']:
        orgs[" ".join(re.split(r"[ ]+",org['display_name'].lower()))] = org['id']
    return orgs

def delete_package(id):
    qry = ckan_rest_url+"package_delete"
    res = requests.post(qry, headers=headers, json={'id':id})
    qry = ckan_rest_url+"dataset_purge"
    res = requests.post(qry, headers=headers, json={'id':id})
    return res.json()

datasets = get_package_list_tag("What\ Works\ Cities")
pprint.pprint(datasets)

for dataset in datasets:
    # print(dataset['id'])
    res = delete_package(dataset['id'])
    print(dataset['id'],res)
