Pre-requisite
-------------
Ensure you have a Python IDE installed on your desktop. The approach I used was to install PyDev for Eclipse as outlined in http://www.pydev.org/download.html

Get the XML Extract
-------------------
In SQL Developer login to the APEX database and write a query to select the required tables with the appropriate where clause. Export the data to an XML file.

Optional: Get the Lookup XML extracts and Update the ETL script
---------------------------------------------------------------
If you don't have the XML files for the lookup tables MDR_ACCESS_LEVEL, MDR_RESOURCE_FORMAT.xml and MDR_UPDATE_FREQUENCY, obtain them similar to the step above and update the path in your local copy of your ETL script import_apex_dataset.py.

Run the ETL Script
------------------
Run the ETL script import_apex_dataset.py with the command line argument pointing to the xml file. For example, if your XML file is called export.xml, run the following statement: import_apex_dataset.py export.xml
Note that your IDE may have a way to specify the command line argument. Please consult the IDE's documentation on how to do that.

Verify the data in CKAN
-----------------------
Verify in CKAN that the data was properly uploaded.