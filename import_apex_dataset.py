import xml.etree.ElementTree
import requests
import pprint
import sys
import string

# Generates the name that is used in the URL of the dataset
def generate_name(title):
    # Include only ascii characters and digits in the name
    stripTitle = title.replace("- ", "")
    validchars = string.ascii_letters + string.digits + " "
    validTitle = ''.join(c for c in stripTitle if c in validchars)
    words = validTitle.lower().split(" ")
    
    #Include at most the first 3 words
    if len(words)<4:
        name = '_'.join(words)
    else:
        name = '_'.join(words[:4])
    return name

def loadLookupTable(lookupTableFile, keyColumn, valColumn):
    tabDict = {}
    root = xml.etree.ElementTree.parse(lookupTableFile).getroot()
    for row in root:
        key = ""
        val = ""
        for column in row.iter('COLUMN'):
            columnName = column.get('NAME')
            if (columnName == keyColumn):
                key = column.text
            elif (columnName == valColumn):
                val = column.text
        tabDict[key] = val
    return tabDict

data_path = "./"

access_level_file = data_path+"MDR_ACCESS_LEVEL.xml"
access_level_key_col = "ACCESS_LEVEL_ID"
access_level_val_col = "ACCESS_LEVEL"
access_level_dict = loadLookupTable(access_level_file, access_level_key_col, access_level_val_col)
print(access_level_dict)

resource_format_file = data_path+"MDR_RESOURCE_FORMAT.xml"
resource_format_key_col = "RESOURCE_FORMAT_ID"
resource_format_val_col = "RESOURCE_FORMAT"
resource_format_dict = loadLookupTable(resource_format_file, resource_format_key_col, resource_format_val_col)
print(resource_format_dict)

update_frequency_file = data_path+"MDR_UPDATE_FREQUENCY.xml"
update_frequency_key_col = "UPDATE_FREQUENCY_ID"
update_frequency_val_col = "UPDATE_FREQUENCY"
update_frequency_dict = loadLookupTable(update_frequency_file, update_frequency_key_col, update_frequency_val_col)
print(update_frequency_dict)

if len(sys.argv) < 2:
    print("import_apex_dataset [xml data file]")
    sys.exit()
    
input_file = sys.argv[1]
print("Reading", input_file)

root = xml.etree.ElementTree.parse(input_file).getroot()
print(root)

header = {'Authorization':'1e41d504-0f52-4141-99a4-5cb1ba2b614e'}
caturl = 'http://coevtubbi1:5000/api/action/package_create'
org_key = '1d9ff121-b83c-4e4e-b14d-ef2c28b02558';
data = []
for row in root:
    dataset = {}
    for column in row.iter('COLUMN'):
        columnName = column.get('NAME')
        if (columnName == 'DATASET_NAME'):
            dataset['name'] = generate_name(column.text)
            dataset['title'] = column.text
        elif (columnName == 'DESCRIPTION'):
            dataset['description'] = column.text
        elif (columnName == 'ACCESS_LEVEL_ID' and column.text):
            dataset['Access Level'] = access_level_dict[column.text]
        elif (columnName == 'RESOURCE_FORMAT_ID' and column.text):
            dataset['Resource Format'] = resource_format_dict[column.text]
        elif (columnName == 'UPDATE_FREQUENCY_ID' and column.text):
            dataset['Update Frequency'] = update_frequency_dict[column.text]
        elif (columnName == 'PERIOD_OF_COVERAGE'):
            dataset['Period of Coverage'] = column.text
        elif (columnName == 'AUTOMATED'):
            dataset['Automated'] = column.text
        elif (columnName == 'DATA_RETENTION'):
            dataset['Data Retention'] = column.text
        elif (columnName == 'METADATA_MAINTAINER'):
            dataset['Metadata Maintainer'] = column.text
        elif (columnName == 'METADATA_MAINTAINER_EMAIL'):
            dataset['Metadata Maintainer Email'] = column.text
        elif (columnName == 'PRIMARY_DATASET'):
            dataset['Primary Dataset'] = 'Yes' if (column.text == 'Y') else 'No'
        elif (columnName == 'OTHER_RESOURCE_FORMAT'):
            dataset['Other Resource Format'] = column.text
        elif (columnName == 'OTHER_UPDATE_FREQUENCY'):
            dataset['Other Update Frequency'] = column.text
        elif (columnName == 'DATASET_UPDATED'):
            dataset['Dataset Updated'] = column.text
        elif (columnName == 'GOVERNED'):
            dataset['Governed'] = column.text
        elif (columnName == 'ADDITIONAL_BUS_INFO'):
            dataset['Additional Business Information'] = column.text
        elif (columnName == 'PUBLISHED'):
            dataset['Published'] = 'Yes' if (column.text == 'Y') else 'No'
        #elif (columnName == 'PUBLISHED_BY_USER'):
        #    dataset['Published by User'] = column.text
        elif (columnName == 'PUBLISHED_DATE'):
            dataset['Published Date'] = column.text
        elif (columnName == 'NEED_APPROVAL'):
            dataset['Need Approval'] = 'Yes' if (column.text == 'Y') else 'No'
        #elif (columnName == 'APPROVED_BY_USER'):
        #    dataset['approved by user'] = column.text
        #elif (columnName == 'APPROVED_DATE'):
        #    dataset['approved date'] = column.text
        #elif (columnName == 'APPROVED'):
        #    dataset['approved'] = 'Yes' if (column.text == 'Y') else 'No'
    data.append(dataset)
    
for ds in data:
    name = ds['name']
    title = ds['title']
    description = ds['description']
    automated = 'Yes' if (ds['Automated'] == 'Y') else 'No'
    governed = 'Yes' if (ds['Governed'] == 'Y') else 'No'
    
    del(ds['name'])
    del(ds['title'])
    del(ds['description'])
    #del(ds['Automated'])
    #del(ds['Governed'])
    
    extras = [{'key':k,'value':v} for k,v in ds.items()]
    print(extras)
    
    dataset_dict = {
        'name': name,
        'title': title,
        'notes': description,
        'automated': automated,
        'governed': governed,
        'extras': extras,
        'owner_org': org_key
    }

    print("Inserting ", name, " with title ", title, "and description ", description)
    r = requests.post(caturl, headers=header, json=dataset_dict)
    res_json = r.json()
    print(name,res_json)
