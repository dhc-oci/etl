import sys
import json
import pprint
import re
import requests
import csv

ckan_rest_url = "http://hubdev.edmonton.ca/api/3/action/"

def get_organization_list():
    qry = ckan_rest_url+"organization_list?all_fields=True"
    res = requests.get(qry)
    orgs = {}
    for org in res.json()['result']:
        orgs[" ".join(re.split(r"[ ]+",org['display_name'].lower()))] = org['id']
    return orgs

if len(sys.argv) < 3:
    print("csvtojson <inputfile.csv> <outputfile.json>")
    sys.exit()

infile = sys.argv[1]
outfile = sys.argv[2]

orgs = get_organization_list()
pprint.pprint(orgs)

data = []
names = {}

with open(infile, encoding = "ISO-8859-1") as f:
    csvdata = csv.DictReader(f)
    csvdata.fieldnames = [s.lower() for s in csvdata.fieldnames]
    for extra in csvdata:
        # extra = line
        org_owner = extra['org_name_path'].split("<")[2].strip().lower()
        if org_owner in orgs:
            org_owner = orgs[org_owner]
        pkg = {'title':extra['dataset_name'],
                'notes':extra['dataset_desc'],
                'access_level':extra['dataset_class_name'],
                'owner_org':org_owner,
                'info_steward': extra['info_steward'],
                'info_custodial': extra['info_custodian'],
                'number_rows': extra['approx_avg_rows'],
                'tags': [{'name':"What Works Cities"}],
                }
        del extra['dataset_name']
        del extra['dataset_desc'],
        del extra['dataset_class_name']
        del extra['org_name_path']
        del extra['info_steward']
        del extra['info_custodian']
        del extra['app_name']
        del extra['app_manufacturer']
        del extra['app_desc']
        del extra['approx_avg_rows']
        name = pkg['title'].lower().split("**")[0]
        name = re.sub(r"[()*']","",name)
        name = re.sub(r"[./\-&, ]","_",name)
        name = re.sub(r"__","_",name)
        n = names.setdefault(name,0)+1
        names[name] = n   
        if n > 1:
            name = "{}_{}".format(name,n)
        pkg['name'] = name
        pkg['extras'] = [{'key': " ".join([w.capitalize() for w in k.split('_')]),'value':v} for k,v in extra.items()]
        if n < 2:
            data.append(pkg)

with open(outfile,"w", encoding = "utf-8") as fout:
    fout.write(json.dumps(data,indent=4))

headers = {'Authorization':'27e9b5ae-309c-4912-8e2a-c935653278c5'}
caturl = ckan_rest_url+'package_create'

with open(outfile) as f:
    pkgs = json.load(f)
    for pkg in pkgs:
        # pprint.pprint(json.dumps(pkg))
        print(json.dumps(pkg))
        # r = requests.post(caturl, data=pkg, headers=headers)
        r = requests.post(caturl, headers=headers, json=pkg)
        res_json = r.json()
        print("-"*30)
        pprint.pprint(res_json)
        print("="*40)
        # sys.exit()
