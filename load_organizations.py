import requests
import json
import csv
import pprint
import sys

headers = {'Authorization':'27e9b5ae-309c-4912-8e2a-c935653278c5'}
caturl = 'http://coevtubbi1/api/action/organization_create'


orgs = {}
new_orgs = {}
with open("organizations.csv") as f:
    forg = csv.reader(f)
    header = next(forg)
    for info in forg:
        #print(info)
        data = {'name':info[1],
                'title':info[2],
                'description':info[3],
                'state':'active'}
        r = requests.post(caturl, headers=headers, json=data)
        if r:
            res_json = r.json()
            print(res_json)
            data['id'] = res_json['result']['id']
            print(data)
            orgs[info[0]]=data
            print(orgs)
        #sys.exit()
fout = open("new_organizations.json","w")
json.dump(orgs,fout)
sys.exit()

data_list = eval(data)


InfoLibrarian = '81f6a45c-536f-4dcf-861f-4de53b9bbd1d'

for d in data_list[9:]:
    name = d['name']
    title = d['title']
    note = d['note']
    tags = []
    if 'tags' in d:
        tags = d['tags']
        del(d['tags'])
    del(d['name'])
    del(d['title'])
    del(d['note'])

    extras = [{'key':k,'value':v} for k,v in d.items()]

    dataset_dict = {
        'name': name,
        'title':title,
        'notes': note,
        'extras': extras,
        #'tags': tags,
        'owner_org': InfoLibrarian,
    }
    pprint.pprint(dataset_dict)
    #i+=1
    r = requests.post(caturl, headers=header, json=dataset_dict)
    res_json = r.json()
    print(name,res_json)
