import requests
import json
import csv
import pprint
import sys

headers = {'Authorization':'27e9b5ae-309c-4912-8e2a-c935653278c5'}
caturl = 'http://coevtubbi1/api/action/package_create'


with open("packages.json") as f:
    pkgs = json.load(f)
    for pkg in pkgs:
        pprint.pprint(pkg)
        r = requests.post(caturl, headers=headers, json=pkg)
        res_json = r.json()
        print("-"*30)
        pprint.pprint(res_json)
        print("="*40)
