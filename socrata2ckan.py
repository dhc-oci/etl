import requests
import pprint
import sys

if len(sys.argv)<2:
    print("socrata2ckan.py <dataset ids>")
    print("  e.g., socrata2ckan.py aart-erty qweo-asdr")
    sys.exit()

datasets = sys.argv[1:]

# query = "http://api.us.socrata.com/api/catalog/v1?domains=data.edmonton.ca&offset=5&only=datasets"
query = "http://api.us.socrata.com/api/catalog/v1"

         # 'offset':"5",
param = {'domains':"data.edmonton.ca",
         'limit':1000,
}
#  'only':"datasets",

res = requests.get(query,params=param)

print(res,res.json().keys(),len(res.json()["results"]))

data = res.json()['results']

header = {'Authorization':'85dd817a-3779-4e8b-8007-6cd0a923365b'}
caturl = 'http://coevtubbi1/api/action/package_create'

opendata = '75df8388-f43e-4746-bc1e-0604855203e3'

for d in data:
    k = d['resource']
    id = k['id']
    # print('Id:',id)
    if id in datasets:
        print(d.keys())
        print(d['permalink'])
        print(d['link'])
        print(d['metadata'])
        name = k['name']
        note = k['description']
        domain = d['metadata']['domain']
        print('id:',id)
        print('Name:',name)
        print('Note:',note)
        url = "https://{}/resource/{}.csv".format(domain,id)
        print("URL:",url)

        dataset_dict = {
            'name': id,
            'title': name,
            'notes': note,
            'owner_org': opendata,
            # 'owner_org': 'd89221ac-28f3-4902-81f7-a5f61baf72cb',
        }
        r = requests.post(caturl, headers=header, json=dataset_dict)
        res_json = r.json()
        # print(res_json)
        if 'result' in res_json:
            dsid = r.json()['result']['id']
            print(dsid,res_json['success'])

        #     crtres = 'http://192.168.1.70/api/action/resource_create'
        #     resource_dict = {
        #         'package_id': dsid,
        #         'url': url,
        #         'format': 'csv',
        #     }
        #     r2 = requests.post(crtres, headers=header, json=resource_dict)
        #     print(id,r2.json()['success'])
