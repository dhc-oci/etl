# Import metadata from XML format into CKAN

import xml.etree.ElementTree
import requests
import pprint
import sys

def printfields(fields):
    for i in range(len(fields)):
        print(i,":",fields[i])

def generate_name(title):
    words = title.lower().split(" ")
    if len(words)<3:
        name = '_'.join(words)
    else:
        name = '_'.join(words[:3])
    return name


if len(sys.argv) < 2:
    print("import_posse2ckan <data.xml>")
    sys.exit()

input_file = sys.argv[1]
print("Reading",input_file)

data = []
lastoname = None
dataset = {}

root = xml.etree.ElementTree.parse(input_file).getroot()

# Processing posse's xml data
catalog = root.find('catalog')
for obj in catalog.iter('object'):
    # print("-"*30)
    # print("name:",obj.attrib['name'])
    # print("id (serogate):",obj.attrib['serogate'])
    # print("description:",obj.attrib['description'])
    dataset = {'name':obj.attrib['serogate'], 'title':obj.attrib['name'], 'note':obj.attrib['description']}
    for prop in obj.iter('property'):
        # print("  -",prop.attrib['name'],":",prop.text.strip())
        dataset[prop.attrib['name']] = prop.text.strip()
    data.append(dataset)
        
# sys.exit()


# for ds in data:
#     pprint.pprint(ds)

# sys.exit()


header = {'Authorization':'6d1dbef4-19e7-457f-9798-276648c50795'}
caturl = 'http://192.168.56.2/api/action/package_create'

InfoLibrarian = '3deb37d3-4bee-4691-be62-a804e5d59fa9'
POSSE = '9da036f2-b51f-4c51-9bd7-31b203c5018b'

id = 1
for d in data:
    # name = generate_name(d['title'])
    # print(name,":",d['title'])

    # name = "infolib%d" % id
    # id += 1

    name = d['name']
    title = d['title']
    note = d['note']
    del(d['name'])
    del(d['title'])
    del(d['note'])

    extras = [{'key':k,'value':v} for k,v in d.items()]
    print(extras)

    dataset_dict = {
        'name': name,
        'title':title,
        'notes': note,
        'extras': extras,
        # 'owner_org': POSSE,
        'owner_org': InfoLibrarian,
        # 'owner_org': 'd74484b2-8b6f-4764-86f3-27b57d1aeb20',
        # 'owner_org': 'd89221ac-28f3-4902-81f7-a5f61baf72cb',
    }
    # print(dataset_dict)
    r = requests.post(caturl, headers=header, json=dataset_dict)
    res_json = r.json()
    print(name,res_json)
    # if 'result' in res_json:
        # dsid = r.json()['result']['id']
        # crtres = 'http://192.168.1.70/api/action/resource_create'
        # resource_dict = {
        #     'package_id': dsid,
        #     'url': url,
        #     'format': 'csv',
        # }
        # r2 = requests.post(crtres, headers=header, json=resource_dict)
        # print(id,r2.json()['success'])
