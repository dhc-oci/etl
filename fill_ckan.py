import requests
import pprint

# query = "http://api.us.socrata.com/api/catalog/v1?domains=data.edmonton.ca&offset=5&only=datasets"
query = "http://api.us.socrata.com/api/catalog/v1"

         # 'offset':"5",
param = {'domains':"data.edmonton.ca",
         'limit':1000,
         'only':"datasets",
}

res = requests.get(query,params=param)

print(res,res.json().keys(),len(res.json()["results"]))

data = res.json()['results']

header = {'Authorization':'6d1dbef4-19e7-457f-9798-276648c50795'}
caturl = 'http://192.168.1.70/api/action/package_create'

for d in data:
    print(d.keys())
    print(d['permalink'])
    print(d['link'])
    print(d['metadata'])
    k = d['resource']
    id = k['id']
    name = k['name']
    note = k['description']
    domain = d['metadata']['domain']
    # print(k.keys())
    print('id:',id)
    print('Name:',name)
    print('Note:',note)
    url = "https://{}/resource/{}.csv".format(domain,id)
    print("URL:",url)
    
    dataset_dict = {
        'name': id,
        'title': name,
        'notes': note,
        'owner_org': 'd74484b2-8b6f-4764-86f3-27b57d1aeb20',
        # 'owner_org': 'd89221ac-28f3-4902-81f7-a5f61baf72cb',
    }
    r = requests.post(caturl, headers=header, json=dataset_dict)
    res_json = r.json()
    if 'result' in res_json:
        dsid = r.json()['result']['id']
        crtres = 'http://192.168.1.70/api/action/resource_create'
        resource_dict = {
            'package_id': dsid,
            'url': url,
            'format': 'csv',
        }
        r2 = requests.post(crtres, headers=header, json=resource_dict)
        print(id,r2.json()['success'])

