import requests
import pprint

with open("infolib.dat") as f:
    data = f.read()

data_list = eval(data)

header = {'Authorization':'1e41d504-0f52-4141-99a4-5cb1ba2b614e'}
caturl = 'http://coevtubbi1:5000/api/action/package_create'

InfoLibrarian = '81f6a45c-536f-4dcf-861f-4de53b9bbd1d'

for d in data_list[9:]:
    name = d['name']
    title = d['title']
    note = d['note']
    tags = []
    if 'tags' in d:
        tags = d['tags']
        del(d['tags'])
    del(d['name'])
    del(d['title'])
    del(d['note'])

    extras = [{'key':k,'value':v} for k,v in d.items()]

    dataset_dict = {
        'name': name,
        'title':title,
        'notes': note,
        'extras': extras,
        #'tags': tags,
        'owner_org': InfoLibrarian,
    }
    pprint.pprint(dataset_dict)
    #i+=1
    r = requests.post(caturl, headers=header, json=dataset_dict)
    res_json = r.json()
    print(name,res_json)
