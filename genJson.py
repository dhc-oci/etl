import json
import csv
import pprint

package = {}
package_extra = {}
orgs = {}

with open("updated_organizations.csv") as csvfile:
    forg = csv.reader(csvfile)
    head = next(forg)
    for data in forg:
        orgs[data[11]] = data[0]

with open("package-test2.csv") as csvfile:
    fpkg = csv.reader(csvfile)
    # print(dir(fpkg))
    # head = [f[1:-1] for f in fpkg.readline().strip().split(',')]
    head = next(fpkg)
    #print(head)
    for data in fpkg:
        fields = data #[f[1:-1] for f in data.strip().split(',')]
        # print(fields)
        info = {}
        # for i in range(len(head)):
            # info[head[i]] = fields[i]
        info['title'] = fields[2]
        info['name'] = fields[1]
        info['notes'] = fields[5]
        info['private'] = fields[15]
        info['type'] = fields[13]
        url = fields[4]
        if url=="(null)":
            url = ""
        info['url'] = fields[4]
        if fields[14]=="(null)":
            owner_org = "3d5ade72-39a4-42e3-93d2-b7923748b171"
        else:
            owner_org = orgs[fields[14]]
        info['owner_org'] = owner_org
        package[fields[0]] = info

with open("package-extra-test2.csv") as fpkg:
    head = [f[1:-1] for f in fpkg.readline().strip().split(',')]
    #print(head)
    for data in fpkg:
        fields = [f[1:-1] for f in data.strip().split(',')]
        # print(fields)
        info = {'key':fields[2], 'value':fields[3]}
        extra = package_extra.setdefault(fields[1],[])
        extra.append(info)
        package_extra[fields[1]] = extra

# pprint.pprint(package_extra)

for id,pkg in package.items():
    if id in package_extra:
        if 'image_url' in package_extra[id][0].values():
            pkg['image_url'] = package_extra[id][0]['value']
        else:
            pkg['extras']=package_extra[id]

val = list(package.values())
print(json.dumps(val,indent=4))
